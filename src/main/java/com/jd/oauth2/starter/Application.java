package com.jd.oauth2.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

/*@SpringBootApplication()
@EnableAutoConfiguration()
@EnableResourceServer*/
@ComponentScan("com.jd.oauth2")
@SpringBootApplication()
@EnableAutoConfiguration()
@EnableResourceServer
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
