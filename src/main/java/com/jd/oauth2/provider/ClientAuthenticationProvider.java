package com.jd.oauth2.provider;

 import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.jd.oauth2.authentication.ClientUsernamePasswordAuthenticationToken;
import com.jd.oauth2.model.Response;
import com.jd.oauth2.services.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * Authentication Provider for 'foo'
 *
 * Created by gdong on 2017/1/24.
 */
@Component
public class ClientAuthenticationProvider implements AuthenticationProvider {
  private final Logger logger = LoggerFactory
      .getLogger(ClientAuthenticationProvider.class);

  @Autowired
  private UserService userService;

  @Override
  public Authentication authenticate(Authentication authentication)
      throws AuthenticationException {
    logger.debug(
        "==== Authenticating using FooAuthenticationProvider: " +   authentication);

    // here goes username/password authentication for Foo
    Response response = userService
        .authenticateUser(String.valueOf(authentication.getPrincipal()),
            String.valueOf(authentication.getCredentials()));

    if (response.isOk()) {
      List<GrantedAuthority> authorities = new ArrayList<>();
      authorities.add(new SimpleGrantedAuthority("READ"));
      authorities.add(new SimpleGrantedAuthority("WRITE"));
      return new ClientUsernamePasswordAuthenticationToken(
          authentication.getPrincipal(), authentication.getCredentials(),
          authorities);
    } else {
      throw new BadCredentialsException("Authentication failed.");
    }
  }

  @Override
  public boolean supports(Class<?> authentication) {
    logger.info("Checking if foo authentication is applicable");
    return ClientUsernamePasswordAuthenticationToken.class
        .isAssignableFrom(authentication);
  }
}
