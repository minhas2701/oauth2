package com.jd.oauth2.provider;

 import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.jd.oauth2.authentication.WizwishUsernamePasswordAuthenticationToken;
import com.jd.oauth2.model.Response;
import com.jd.oauth2.services.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * Authentication Provider for 'bar'
 *
 * Created by gdong on 2017/1/24.
 */
@Component
public class WizwishAuthenticationProvider implements AuthenticationProvider {
  private final Logger logger = LoggerFactory
      .getLogger(WizwishAuthenticationProvider.class);

  @Autowired
  private UserService userService;

  @Override
  public Authentication authenticate(Authentication authentication)
      throws AuthenticationException {
    logger.debug(
        "==== Authenticating using BarAuthenticationProvider: " +
            authentication);

    // here goes username/password authentication for Foo
    Response response = userService
        .authenticateBar(String.valueOf(authentication.getPrincipal()),
            String.valueOf(authentication.getCredentials()));

    if (response.isOk()) {
      List<GrantedAuthority> authorities = new ArrayList<>();
      authorities.add(new SimpleGrantedAuthority("BAR_READ"));
      authorities.add(new SimpleGrantedAuthority("BAR_WRITE"));
      return new WizwishUsernamePasswordAuthenticationToken(
          authentication.getPrincipal(), authentication.getCredentials(),
          authorities);
    } else {
      throw new BadCredentialsException("Authentication failed.");
    }
  }

  @Override
  public boolean supports(Class<?> authentication) {
    logger.info("Checking if bar authentication is applicable");
    return WizwishUsernamePasswordAuthenticationToken.class
        .isAssignableFrom(authentication);
  }
}
