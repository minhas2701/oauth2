package com.jd.oauth2.config;

import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.context.annotation.Configuration;
 import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
 
import com.jd.oauth2.provider.WizwishAuthenticationProvider;
import com.jd.oauth2.provider.ClientAuthenticationProvider;

 
import javax.servlet.http.HttpServletResponse;

 @Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
/*	 @Autowired
	 DataSource dataSource;
    @Autowired
    public void globalUserDetails(AuthenticationManagerBuilder auth) throws Exception {
    	 auth.jdbcAuthentication().dataSource(dataSource) .usersByUsernameQuery(
    			   "select username,password, enabled from users where username=?")
    	  .authoritiesByUsernameQuery(
    	   "select username, role from user_roles where username=?");
     

    	 auth.jdbcAuthentication()
                .withUser("user").password("password").roles("USER")
                .and()
                .withUser("app_client").password("nopass").roles("USER")
                .and()
                .withUser("admin").password("password").roles("ADMIN");
    }
    @Bean(name = "dataSource")
    public DriverManagerDataSource dataSource() {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName("com.mysql.jdbc.Driver");
        driverManagerDataSource.setUrl("jdbc:mysql://localhost:3306/auth2");
        driverManagerDataSource.setUsername("local");
        driverManagerDataSource.setPassword("local");
        return driverManagerDataSource;
    } 
	 
	/* @Autowired
	    public void globalUserDetails(AuthenticationManagerBuilder auth) throws Exception {
	    	 
		 
	    	 auth.inMemoryAuthentication()
	                .withUser("user").password("password").roles("USER")
	                .and()
	                .withUser("app_client").password("nopass").roles("USER")
	                .and()
	                .withUser("admin").password("password").roles("ADMIN");
	    }
	*/ 
	 @Autowired
	  private ClientAuthenticationProvider fooAuthenticationProvider;

	  @Autowired
	  private WizwishAuthenticationProvider barAuthenticationProvider;
	    @Autowired
	    public void globalUserDetails(AuthenticationManagerBuilder auth) throws Exception {
	    	 
		  auth.authenticationProvider(fooAuthenticationProvider)
	        .authenticationProvider(barAuthenticationProvider).inMemoryAuthentication();;
	    }  
	   @Override
	  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	    auth.authenticationProvider(fooAuthenticationProvider)
	        .authenticationProvider(barAuthenticationProvider).inMemoryAuthentication();
	     ;
//	        .withUser("sample1")
//	        .password("donggan")
//	        .authorities("READ")
//	        .and()
//	        .withUser("sample2")
//	        .password("donggan")
//	        .authorities("READ", "WRITE");

	  } 
    @Override
    protected void configure(HttpSecurity http) throws Exception {
       /* http
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS).permitAll()
               // .antMatchers(HttpMethod.POST, "/auth/oauth/token/**").permitAll()
                .anyRequest().authenticated()
                .and().httpBasic()
                .and().csrf().disable();
        */
        
        http.csrf().disable()
        .exceptionHandling()
        .authenticationEntryPoint((request, response, authException) -> response.sendError(
            HttpServletResponse.SC_UNAUTHORIZED))
        .and()
        .authorizeRequests()
        .antMatchers("/**").authenticated()
        .and()
        .httpBasic();
        
     // Custom JWT based security filter
   //     http
              //  .addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);

        // disable page caching
       
    }
   /* @Bean
    public JwtAuthenticationTokenFilter authenticationTokenFilterBean() throws Exception {
        JwtAuthenticationTokenFilter authenticationTokenFilter = new JwtAuthenticationTokenFilter();
        authenticationTokenFilter.setAuthenticationManager(authenticationManager());
        authenticationTokenFilter.setAuthenticationSuccessHandler(new JwtAuthenticationSuccessHandler());
        
        return authenticationTokenFilter;
    }*/
    /*@Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    } */
    
    /*@Autowired
    private JwtAuthenticationEntryPoint unauthorizedHandler;

    @Autowired
    private JwtAuthenticationProvider authenticationProvider;

    @Bean
    @Override
    public AuthenticationManager authenticationManager() throws Exception {

        return new ProviderManager(Arrays.asList(authenticationProvider));
    }*/
}
