package com.jd.oauth2.config;

 

 
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
 import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationManager;
 import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.refresh.RefreshTokenGranter;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import com.jd.oauth2.provider.CustomResourceOwnerPasswordTokenGranter;
import com.jd.oauth2.provider.CustomResourceRefreshOwnerPasswordTokenGranter;
import com.jd.oauth2.services.HybridUserDetailsService;

import io.jsonwebtoken.SignatureAlgorithm;
 

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

	@Value("${resource.id:spring-boot-application}")
    private String resourceId;
    
    @Value("${access_token.validity_period}")
    int accessTokenValiditySeconds ;
    @Value("${refresh_token.validity_period}")
    int refreshTokenValiditySeconds ;
   
    
    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    private AuthenticationManager authenticationManager;
    //@Autowired
   // private DataSource dataSource;
     @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        return new JwtAccessTokenConverter();
    } 
     @Autowired
     private  HybridUserDetailsService userDetailsService;
    /* @Bean
    public JdbcTokenStore tokenStore() {
    return new JdbcTokenStore(dataSource);
    } 
    @Bean(name = "dataSource")
    public DriverManagerDataSource dataSource() {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName("com.mysql.jdbc.Driver");
        driverManagerDataSource.setUrl("jdbc:mysql://localhost:3306/auth2");
        driverManagerDataSource.setUsername("local");
        driverManagerDataSource.setPassword("local");
        return driverManagerDataSource;
    }*/
   /* @Bean
    protected AuthorizationCodeServices authorizationCodeServices() {
    return new JdbcAuthorizationCodeServices(dataSource);
    }*/
    
   /* @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        DefaultTokenServices tokenServices = new DefaultTokenServices();
        tokenServices.setSupportRefreshToken(true);
        tokenServices.setAccessTokenValiditySeconds(300);
        tokenServices.setRefreshTokenValiditySeconds(6000);
        tokenServices.setTokenStore(new JdbcTokenStore(dataSource));
        return tokenServices;
    }*/

    
  /*  @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
            .authenticationManager(this.authenticationManager)
            .tokenEnhancer(jwtTokenEnhancer());
        
        
    }*/
     @Bean
     public TokenStore tokenStore() {
       return new JwtTokenStore(jwtTokenEnhancer());
     }
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints)
        throws Exception {
      endpoints.tokenStore(tokenStore())
          .tokenEnhancer(jwtTokenEnhancer())
          .authenticationManager(authenticationManager)
          .userDetailsService(userDetailsService);

      List<TokenGranter> tokenGranters = new ArrayList<>();
      tokenGranters
          .add(new CustomResourceOwnerPasswordTokenGranter(authenticationManager,
              endpoints.getTokenServices(), endpoints.getClientDetailsService(),
              endpoints.getOAuth2RequestFactory()));
      
      
     /* tokenGranters.add(new RefreshTokenGranter(endpoints.getTokenServices(),
          endpoints.getClientDetailsService(),
          endpoints.getOAuth2RequestFactory())); */
      tokenGranters.add(new CustomResourceRefreshOwnerPasswordTokenGranter(authenticationManager,
              endpoints.getTokenServices(), endpoints.getClientDetailsService(),
              endpoints.getOAuth2RequestFactory()));
      endpoints.tokenGranter(new CompositeTokenGranter(tokenGranters));
    }
    @Bean
    protected JwtAccessTokenConverter jwtTokenEnhancer() {
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(
                new ClassPathResource("jwt.jks"), "mySecretKey".toCharArray());
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
      
       // converter.setKeyPair(keyStoreKeyFactory.getKeyPair("jwt"));
        converter.setSigningKey(secret);
        //converter.setVerifierKey(SignatureAlgorithm.HS512);
       // .signWith(SignatureAlgorithm.HS512, secret)
        
        return converter;
    }
   /* @Bean
    TokenStore tokenStore() {
        return new JwtTokenStore(jwtTokenEnhancer());
    }*/
    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        oauthServer.tokenKeyAccess("isAnonymous() || hasAuthority('ROLE_TRUSTED_CLIENT')")
            .checkTokenAccess("hasAuthority('ROLE_TRUSTED_CLIENT')");
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
    	clients.inMemory()
    	  .withClient("normal-app")
                .authorizedGrantTypes("authorization_code", "implicit")
                .authorities("ROLE_CLIENT")
                .scopes("read", "write")
                .resourceIds(resourceId)
                .accessTokenValiditySeconds(accessTokenValiditySeconds)
        .and()
            .withClient("trusted-app")
                .authorizedGrantTypes("client_credentials","refresh_token", "password")
                .authorities("ROLE_TRUSTED_CLIENT")
                .scopes("read", "write")
                .resourceIds(resourceId)
                .accessTokenValiditySeconds(accessTokenValiditySeconds).refreshTokenValiditySeconds(refreshTokenValiditySeconds)
                .secret("secret");
  /*      .withClient("foo_app")
        .autoApprove(false)
        .authorities("FOO_READ", "FOO_WRITE")
        .authorizedGrantTypes("authorization_code", "refresh_token", "implicit",
            "password", "client_credentials")
        .scopes("FOO")
        .and()
        .withClient("bar_app")
        .autoApprove(false)
        .authorities("BAR_READ", "BAR_WRITE")
        .authorizedGrantTypes("authorization_code", "refresh_token", "implicit",
            "password", "client_credentials")
        .scopes("BAR");*/
    }

   /* private static class MyTokenEnhancer implements TokenEnhancer {
        @Override
        public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
            final User user = (User) authentication.getPrincipal();
            final Map<String, Object> additionalInfo = new HashMap<>();
            additionalInfo.put("userId", user.getUserId());
            additionalInfo.put("companyId", user.getCompanyId());
            additionalInfo.put("roles", user.getAuthorities());
            ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
            return accessToken;
        }

		 
    }*/
}



