package com.jd.oauth2.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
 import org.springframework.web.bind.annotation.RestController;

import com.jd.oauth2.model.Account;
import com.jd.oauth2.model.Response;
import com.jd.oauth2.services.UserService;
 
@RestController
@RequestMapping("/resources")
public class ResourceController {
@Autowired
UserService userService;
    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value="user", method=RequestMethod.GET)
    public String helloUser() {
        return "hello user";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value="admin", method=RequestMethod.GET)
    public String helloAdmin() {
        return "hello admin";
    }

    @PreAuthorize("hasRole('ROLE_CLIENT')")
    @RequestMapping(value="client", method=RequestMethod.GET)
    public String helloClient() {
        return "hello user authenticated by normal client";
    }

    @PreAuthorize("hasRole('ROLE_TRUSTED_CLIENT')")
	@RequestMapping(value="trusted_client", method=RequestMethod.GET)
    public String helloTrustedClient() {
        return "hello user authenticated by trusted client";
    }

    @RequestMapping(value="principal", method=RequestMethod.GET)
    public UsernamePasswordAuthenticationToken getPrincipal() {
		
		 Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	        
		 return new UsernamePasswordAuthenticationToken(authentication.getPrincipal(),authentication.getCredentials(),authentication.getAuthorities());

		
        //Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
       // return principal;
    }

    @RequestMapping(value="account", method=RequestMethod.GET)
    public Account getAccount() {
		
		 Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		 UsernamePasswordAuthenticationToken  user = new UsernamePasswordAuthenticationToken(authentication.getPrincipal(),authentication.getCredentials(),authentication.getAuthorities());
  
 	       //Account.SecurityRole.values()
 	       Account.SecurityRole[] roles =new Account.SecurityRole[user.getAuthorities().size()];
 	        int i=0;
 	        for(GrantedAuthority role : user.getAuthorities()) {
 	            //roles[i++] = User.SecurityRole.valueOf(role);

 	            roles[i++] = Account.SecurityRole.valueOf(role.toString());
 	        }
 	        Account account = new Account(user.getName(), user.getName(), user.getCredentials().toString(), roles);

 	        return account;
 	        
 	        
         //Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
       // return principal;
    }
    

	@RequestMapping(value="roles", method=RequestMethod.GET)
    public Object getRoles() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities();
    }
	
	 @RequestMapping(value = "register", method = RequestMethod.POST)
    public Object register( @RequestParam(value = "j_username") String username,
             @RequestParam(value = "j_password") String password,
             @RequestParam(value = "j_role") String role) {
		 
		 Response respone = userService.registerUser(username, password,role); 
		 Result result  = new Result(true,"Succeful", "");
		 if(!respone.isOk()){
			 result = new Result(false,"fail", "");
		 }
			return result;
    }

}
