package com.jd.oauth2.services;

 

 import java.util.Map;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.jd.oauth2.model.Response;

 
/**
 *
 * a user service that can verify from two different data source
 *
 * Created by gdong on 2017/1/24.
 */

  
 @Service("UserService")
 @Component
public class UserService {
	 @Autowired
	 DataBaseService dataBaseService;
	 
  /**
   * authenticates from 'Foo' data source
   *
   * @param username
   * @param password
   * @return
   */
  public Response authenticateUser(String username, String password) {
    // here goes your actual username && password verification logic
	  Map<String, Object> result =   dataBaseService.userResult(username,password);
	  return result != null ?
		        Response.ok() :
		        Response.fail();
	  
	  
    /*return "gan".equals(username) && "foo".equals(password) ?
        Response.ok() :
        Response.fail();*/
  }

  
  public Response registerUser(String username, String password,String role) {
	    // here goes your actual username && password verification logic
		 boolean result =   dataBaseService.createUser(  username ,  password,  role);
		  return result ?
			        Response.ok() :
			        Response.fail();
		  
		  
	    /*return "gan".equals(username) && "foo".equals(password) ?
	        Response.ok() :
	        Response.fail();*/
	  }
  
  
  public Response loadFooUser(String username) {
 	  Map<String, Object> result =   dataBaseService.userResult(username);
	  return result != null ?
		        Response.ok() :
		        Response.fail();
	  
	  
	 
  }

  /**
   * authenticates from 'Bar' data source
   *
   * @param username
   * @param password
   * @return
   */
  public Response authenticateBar(String username, String password) {
    // here goes your actual username && password verification logic
    return "dong".equals(username) && "bar".equals(password) ?
        Response.ok() :
        Response.fail();
  }

  public Response loadBarUser(String username) {
    return "dong".equals(username) ? Response.ok() : Response.fail();
  }

}
