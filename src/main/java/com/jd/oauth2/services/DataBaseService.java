package com.jd.oauth2.services;

 
 
 
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.db.helper.DBConnectionManager;
import com.db.helper.DBHelper;
import com.db.helper.DatabaseException;

@Service
 
public class DataBaseService {
	
	//private static Logger logger = Logger.getLogger(DataBaseService.class);
	private static final Logger logger = Logger.getLogger(DataBaseService.class);

	public DataBaseService (){
		
	}
	 public Map<String, Object>  userResult(String user ) {
		 
		 java.sql.Connection conn = null;
			try {
			         conn = DBConnectionManager.getInstance().getConnection();
			        
			       //  Map<String, Object> result =  DBHelper.getInstance().firstRow("SELECT `users`.`password`,`users`.`enabled`  , `user_roles`.`role` FROM `auth2`.`user_roles` INNER JOIN `auth2`.`users`  ON (`user_roles`.`username` = `users`.`username`) where users.username=?",conn,user);
			         Map<String, Object> result =  DBHelper.getInstance().firstRow("SELECT  *from users where username=?",conn,user);

			         
			         logger.info("userResult table:" + result);
			 
			  
			 
			 return result;
			  
			} catch (DatabaseException e) {
				// TODO Auto-generated catch block
				logger.error("query excuted faild: " + e.getMessage());

				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("query excuted faild: " + e.getMessage());
				e.printStackTrace();
			}finally{
				try {
					DBConnectionManager.getInstance().returnConnection(conn);
				} catch (DatabaseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			return null;
	 }
	 
	 
public Map<String, Object>  userResult(String user ,String password) {
		 
		 java.sql.Connection conn = null;
			try {
			         conn = DBConnectionManager.getInstance().getConnection();
			        
			       //  Map<String, Object> result =  DBHelper.getInstance().firstRow("SELECT `users`.`password`,`users`.`enabled`  , `user_roles`.`role` FROM `auth2`.`user_roles` INNER JOIN `auth2`.`users`  ON (`user_roles`.`username` = `users`.`username`) where users.username=?",conn,user);
			 
			         Map<String, Object> result =  DBHelper.getInstance().firstRow("SELECT  *from users where username=?",conn,user);
			     	
			         logger.info("userResult table:" + result);
			 
			  
			 
			 return result;
			  
			} catch (DatabaseException e) {
				// TODO Auto-generated catch block
				logger.error("query excuted faild: " + e.getMessage());

				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("query excuted faild: " + e.getMessage());
				e.printStackTrace();
			}finally{
				try {
					DBConnectionManager.getInstance().returnConnection(conn);
				} catch (DatabaseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			return null;
	 }	
public boolean createUser(String user ,String password,String role) {
	 
	 java.sql.Connection conn = null;
		try {
		         conn = DBConnectionManager.getInstance().getConnection();
		        
		         int result =  DBHelper.getInstance().executeDml("INSERT INTO users (username,PASSWORD,role)VALUES(?,?,?)",conn,user,password,role);
		 logger.info("userResult table:" + result);
		 
		  
		 
		 return result == 1?true:false;
		  
		} catch (DatabaseException e) {
			// TODO Auto-generated catch block
			logger.error("query excuted faild: " + e.getMessage());

			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("query excuted faild: " + e.getMessage());
			e.printStackTrace();
		}finally{
			try {
				DBConnectionManager.getInstance().returnConnection(conn);
			} catch (DatabaseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return false;
}	
}
