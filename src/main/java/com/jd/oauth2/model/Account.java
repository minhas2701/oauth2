package com.jd.oauth2.model;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.GrantedAuthority;

 
  
public class Account {
    private static final String SALT = "cewuiqwzie";

	    String username;
 	    String password;
 
 	    private SecurityRole[] roles;

 	   public SecurityRole[] getRole() {
 	        return roles;
 	    }

 	   public Account() {
 	    }

 	    public Account(String username, String name, String password) {
 	        this.username = username;
  	        this.password = password;
 	    }

 	    public Account(String username, String name, String password, SecurityRole... roles) {
 	        this.username = username;
  	        this.password = encode(password);
 	        this.roles = roles;
 	    }
 	   public String getUsername() {
 	        return username;
 	    }
 	    public String setUsername() {
 	        return username;
 	    }
 	    public String getPassword() {
 	        return password;
 	    }

 	    public void updatePassword(String old, String newPass1, String newPass2) {
 	        if (!password.equals(encode(old))) {
 	            throw new IllegalArgumentException("Existing Password invalid");
 	        }
 	        if (!newPass1.equals(newPass2)) {
 	            throw new IllegalArgumentException("New Passwords don't match");
 	        }
 	        this.password = encode(newPass1);
 	    }
 	    
 	   private String encode(String password) {
 	        return new Md5PasswordEncoder().encodePassword(password, SALT);
 	    }

 	    
 	    
 	    
 	   @Override
 	    public boolean equals(Object o) {
 	        if (this == o) {
 	            return true;
 	        }
 	        if (!(o instanceof Account)) {
 	            return false;
 	        }

 	       Account account = (Account) o;

 	        if (username != null ? !username.equals(account.username) : account.username != null) {
 	            return false;
 	        }

 	        return true;
 	    }
 	   
 	    @Override
 	    public int hashCode() {
 	        return username != null ? username.hashCode() : 0;
 	    }
 	   public enum SecurityRole  {
 	        ROLE_USER, ROLE_ADMIN,READ,WRITE;;

  	        public String getAuthority() {
 	            return name();
 	        }
 	    }
	
}
